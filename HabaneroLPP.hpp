
// HabaneroLPP
// https://gitlab.com/lowswaplab/habanerolpp
// Copyright © 2020 Low SWaP Lab lowswaplab.com

#ifndef HABANEROLPP_HPP
  #define HABANEROLPP_HPP

  #if defined(ARDUINO)
    #include <Arduino.h>
  #elif defined(HAVE_LINUX)
    #include <string>
    #define F(x) (x)
    #define String std::string
  #endif

  #include <stdint.h>
  #include <string.h>
  #include <math.h>

  #include <errno.h>
  #ifndef EBADMSG
    #define EBADMSG (74)
  #endif

  /*! \brief Low SWaP Lab namespace
   *
   *  Low SWaP Lab software can be found at https://www.lowswaplab.com/
   */
  namespace LowSWaPLab
    {
    //! Packet types
    enum types
      {
      LPP_TYPE_ERROR=0,
      LPP_TYPE_ATTITUDE_HEADING,
      LPP_TYPE_ATTITUDE_PITCH,
      LPP_TYPE_ATTITUDE_ROLL,
      LPP_TYPE_ATTITUDE_TRACK,
      LPP_TYPE_ATTITUDE_XYZ,
      LPP_TYPE_STRING,
      LPP_TYPE_ENVIRO_TEMPERATURE,
      LPP_TYPE_ENVIRO_HUMIDITY,
      LPP_TYPE_ENVIRO_PRESSURE,
      LPP_TYPE_ENVIRO_THP,
      LPP_TYPE_GPS_PDOP,
      LPP_TYPE_GPS_POSITION,
      LPP_TYPE_GPS_SATELLITES,
      LPP_TYPE_LORA_NEIGHBOR,
      LPP_TYPE_LORA_NOISEFLOOR,
      LPP_TYPE_LORA_RSSI,
      LPP_TYPE_LORA_SNR,
      LPP_TYPE_SPEED_GROUND,
      LPP_TYPE_SPEED_VERTICAL,
      LPP_TYPE_SYS_SERIALNUM,
      LPP_TYPE_SYS_UPTIME,
      LPP_TYPE_TIMEOUT,
      LPP_TYPE_WIFI_RSSI,
      LPP_TYPE_VOLTAGE_DC,
      LPP_TYPE_RPC_BLINK,
      LPP_TYPE_RPC_HLPP_VERSION_REPLY,
      LPP_TYPE_RPC_HLPP_VERSION_REQUEST,
      LPP_TYPE_RPC_PING_REPLY,
      LPP_TYPE_RPC_PING_REQUEST
      };

    //! This union is primarily for object internal use only.
    //! It is available for advanced uses.
    typedef union
      {
      float f;
      uint32_t u;
      int32_t i;
      } HabaneroLPP_value_t;

    //! This struct is primarily for object internal use only.
    //! It is available for advanced uses.
    //! Note: LoRa maximum packet size for SF12 should be 51 bytes.
    typedef struct
      {
      uint8_t csum;
      uint8_t type;
      HabaneroLPP_value_t value0;
      HabaneroLPP_value_t value1;
      HabaneroLPP_value_t value2;
      } HabaneroLPP_t;

    class HabaneroLPP
      {
      public:
        //! Object constructor.
        HabaneroLPP();

        //! Object destructor.
        ~HabaneroLPP();

        //! Get MQTT topic of packet type.
        //! \return MQTT topic of packet.
        String topic() const;

        //! Get type of packet.
        //! \return Type of packet.
        uint8_t type() const;

        //! Get size of packed packet (note: not the size of a memory
        //! aligned HabaneroLPP_t struct).
        //! \return Size of packed packet (bytes).
        uint8_t size() const;

        //! Get values as string.
        //! \return Values as string.
        String values() const;

        //! Get value 0 as float.
        //! \return Value 0 as float.
        float value0f() const;

        //! Get value 1 as float.
        //! \return Value 1 as float.
        float value1f() const;

        //! Get value 2 as float.
        //! \return Value 2 as float.
        float value2f() const;

        //! Get value 0 as uint32_t.
        //! \return Value 0 as uint32_t.
        uint32_t value0u() const;

        //! Get value 1 as uint32_t.
        //! \return Value 1 as uint32_t.
        uint32_t value1u() const;

        //! Get value 2 as uint32_t.
        //! \return Value 2 as uint32_t.
        uint32_t value2u() const;

        //! Get value 0 as int32_t.
        //! \return Value 0 as int32_t.
        int32_t value0i() const;

        //! Get value 1 as int32_t.
        //! \return Value 1 as int32_t.
        int32_t value1i() const;

        //! Get value 2 as int32_t.
        //! \return Value 2 as int32_t.
        int32_t value2i() const;

        //! Reset object to default state.
        void reset();

        //! Return a copy of the current LPP packet (not packed).
        //! \param packet Packet into which LPP packet is copied.
        //! \return LPP packet copied into packet parameter.
        void lpp(HabaneroLPP_t *packet);

        //! Create debug output for a buffer
        //! \param buffer Buffer to parse.
        //! \param size Size of buffer (bytes).
        //! \return String with hex representation of buffer.
        String debug(const uint8_t *buffer, const uint8_t size);

        //! Parse an incoming packet.
        //! \param buffer Buffer to parse.
        //! \param size Size of buffer (bytes).
        //! \return True on success, false on error.
        bool parse(const uint8_t *buffer, const uint8_t size);

        //! Create an outgoing packed packet.
        //! \param buffer Buffer into which packet is copied.
        //! \param size Size of buffer (bytes).
        //! \return True on success with size set to length of packet,
        //!         false on error.
        bool packet(uint8_t *buffer, uint8_t *size);

        //! Generate an 8-bit checksum.
        //! \param buffer Buffer to checksum.
        //! \param size Size of buffer (bytes).
        //! \return 8-bit checksum.
        uint8_t checksum(const uint8_t *buffer, const uint8_t size);

        //! Set a packet value 0.  This is primarily for object internal use.
        //! It is available for advanced uses.
        //! \param type Type of packet.
        //! \param value0 Value of packet.
        //! \return True on success, false on error.
        bool set0(const uint8_t type, const float value0);

        //! Set a packet value 0.  This is primarily for object internal use.
        //! It is available for advanced uses.
        //! \param type Type of packet.
        //! \param value0 Value of packet.
        //! \return True on success, false on error.
        bool set0(const uint8_t type, const uint32_t value0);

        //! Set a packet value 0.  This is primarily for object internal use.
        //! It is available for advanced uses.
        //! \param type Type of packet.
        //! \param value0 Value of packet.
        //! \return True on success, false on error.
        bool set0(const uint8_t type, const int32_t value0);

        //! Set a packet value 1.  This is primarily for object internal use.
        //! It is available for advanced uses.
        //! \param type Type of packet.
        //! \param value1 Value of packet.
        //! \return True on success, false on error.
        bool set1(const uint8_t type, const float value1);

        //! Set a packet value 1.  This is primarily for object internal use.
        //! It is available for advanced uses.
        //! \param type Type of packet.
        //! \param value1 Value of packet.
        //! \return True on success, false on error.
        bool set1(const uint8_t type, const uint32_t value1);

        //! Set a packet value 1.  This is primarily for object internal use.
        //! It is available for advanced uses.
        //! \param type Type of packet.
        //! \param value1 Value of packet.
        //! \return True on success, false on error.
        bool set1(const uint8_t type, const int32_t value1);

        //! Set a packet value 2.  This is primarily for object internal use.
        //! It is available for advanced uses.
        //! \param type Type of packet.
        //! \param value2 Value of packet.
        //! \return True on success, false on error.
        bool set2(const uint8_t type, const float value2);

        //! Set a packet value 2.  This is primarily for object internal use.
        //! It is available for advanced uses.
        //! \param type Type of packet.
        //! \param value2 Value of packet.
        //! \return True on success, false on error.
        bool set2(const uint8_t type, const uint32_t value2);

        //! Set a packet value 2.  This is primarily for object internal use.
        //! It is available for advanced uses.
        //! \param type Type of packet.
        //! \param value2 Value of packet.
        //! \return True on success, false on error.
        bool set2(const uint8_t type, const int32_t value2);

        //! Set heading (direction facing).
        //! \param heading Heading (0° <= heading < 360°).
        //! \return True on success, false on error.
        bool setAttitudeHeading(const float heading);

        //! Set roll (negative is left wing down/right wing up,
        //  positive is left wing up/right wing down).
        //! \param roll Roll (-90° <= roll <= 90°).
        //! \return True on success, false on error.
        bool setAttitudeRoll(const float roll);

        //! Set pitch (negative is nose down, positive is nose up).
        //! \param pitch pitch (-90° <= pitch <= 90°).
        //! \return True on success, false on error.
        bool setAttitudePitch(const float pitch);

        //! Set track (direction of movement).
        //! \param track Track (0° <= track < 360°).
        //! \return True on success, false on error.
        bool setAttitudeTrack(const float track);

        //! Set X, Y, Z (or roll, pitch, yaw).
        //! \param x (-360° < x < 360°).
        //! \param y (-360° < y < 360°).
        //! \param z (-360° < z < 360°).
        //! \return True on success, false on error.
        bool setAttitudeXYZ(const float x, const float y, const float z);

        //! Set humidity.
        //! \param humidity Humidity (0% <= humidity <= 100%).
        //! \return True on success, false on error.
        bool setEnviroHumidity(const float humidity);

        //! Set pressure.
        //! \param pressure Pressure
        //!                 (between 0 hPa <= pressure <= 3.4E38 hPa).
        //! \return True on success, false on error.
        bool setEnviroPressure(const float pressure);

        //! Set temperature.
        //! \param temperature Temperature
        //!                    (-273.15°C <= temperature <= 1.417E32°C).
        //! \return True on success, false on error.
        bool setEnviroTemperature(const float temperature);

        //! Set temperature, humidity, and pressure
        //! \param temperature Temperature
        //!                    (-273.15°C <= temperature <= 1.417E32°C).
        //! \param humidity Humidity (0% <= humidity <= 100%).
        //! \param pressure Pressure
        //!                 (between 0 hPa <= pressure <= 3.4E38 hPa).
        //! \return True on success, false on error.
        bool setEnviroTHP(const float temperature, const float humidity,
                          const float pressure);

        //! Set GPS PDOP.
        //! \param pdop PDOP (pdop >= 0 meters)
        //! \param hdop HDOP (hdop >= 0 meters)
        //! \param vdop VDOP (vdop >= 0 meters)
        //! \return True on success, false on error.
        bool setGPSpdop(const uint32_t pdop, const uint32_t hdop,
                        const uint32_t vdop);

        //! Set GPS position.
        //! \param latitude Latitude (WGS-84 -90° <= latitude <= 90°).
        //! \param longitude Longitude (WGS-84 -180° <= longitude <= 180°).
        //! \param altitude Optional altitude (meters MSL).
        //! \return True on success, false on error.
        bool setGPSposition(const float latitude, const float longitude,
                            const float altitude=INFINITY);

        //! Set GPS satellites.
        //! \param satellites Number of satellites.
        //! \return True on success, false on error.
        bool setGPSsatellites(const uint32_t satellites);

        //! Set LoRa neighbor RSSI.
        //! \param addr Neighbor address/id.
        //! \param rssi RSSI (-179 dBm <= RSSI <= 0 dBm).
        //! \return True on success, false on error.
        bool setLoRaNeighbor(const uint32_t addr, const float rssi);

        //! Set LoRa noise floor.
        //! \param noise floor Noise floor
        //!                    (-179 dBm <= noise floor <= 0 dBm).
        //! \return True on success, false on error.
        bool setLoRaNoisefloor(const float noisefloor);

        //! Set LoRa RSSI.
        //! \param rssi RSSI (-179 dBm <= RSSI <= 0 dBm).
        //! \return True on success, false on error.
        bool setLoRaRSSI(const float rssi);

        //! Set LoRa SNR.
        //! \param saddr Source address.
        //! \param daddr Destination address.
        //! \param snr SNR (-179 dBm <= SNR <= 179 dBm).
        //! \return True on success, false on error.
        bool setLoRaSNR(const uint32_t saddr, const uint32_t daddr,
                        const float snr);

        //! Set ground speed.
        //! \param speed Ground speed (speed >= 0 kph).
        //! \return True on success, false on error.
        bool setSpeedGround(const float speed);

        //! Set vertical speed.
        //! \param speed Vertical speed (speed < 0 m/s descending,
        //!              speed > 0 m/s ascending).
        //! \return True on success, false on error.
        bool setSpeedVertical(const float speed);

        //! Set system serial number.
        //! \param sernum Serial number.
        //! \return True on success, false on error.
        bool setSysSerialNum(const uint32_t sernum=0);

        //! Set system uptime.
        //! \param uptime Uptime (uptime > 0 seconds).  If uptime is 0,
        //!               uptime is automatically retrieved from the system.
        //!               XXX Uptime on Linux is uint64_t!
        //! \return True on success, false on error.
        bool setSysUptime(const uint32_t uptime=0);

        //! Set timeout of data
        //! \param timeout Timeout (timeout >= 0 seconds).
        //! \return True on success, false on error.
        bool setTimeout(const uint32_t timeout);

        //! Set voltage DC.
        //! \param voltage Voltage (volts DC).
        //! \return True on success, false on error.
        bool setVoltageDC(const float voltage);

        //! Set WiFi RSSI.
        //! \param rssi RSSI (-179 dBm <= RSSI <= 0 dBm).
        //! \return True on success, false on error.
        bool setWiFiRSSI(const float rssi);

        //! Set a blink command.
        bool setRPCblink();

        //! Set a HabaneroLPP version reply.
        bool setRPChlppVersionReply();

        //! Set a HabaneroLPP version request.
        bool setRPChlppVersionRequest();

        //! Set a ping reply.
        //! \param time Time (pick the units (e.g. ms, μs, ns), just be
        //!             consistent with setRPCpingRequest()!).
        //! \param rssi SNR (dBm) of ping request received.
        bool setRPCpingReply(const uint32_t time,
                             const float snr);

        //! Set a ping request.
        //! \param time Time (pick the units (e.g. ms, μs, ns), just be
        //!             consistent with setRPCpingReply()!).  If time is 0,
        //!             the the current time is automatically retrieved
        //!             from the system.  For Arduino-based platforms,
        //!             time is set to microseconds using "micros()".
        //!             For Linux, time is set to microseconds using
        //!             "gettimeofday()".
        bool setRPCpingRequest(const uint32_t time=0);

      private:
        HabaneroLPP_t _lpp;
      };

    // Payload sizes (bytes)
    #define LPP_SIZE_TYPE            (1) // uint8_t
    #define LPP_SIZE_CHECKSUM        (1) // uint8_t
    #define LPP_SIZE_PAYLOAD_1       (4) // value0
    #define LPP_SIZE_PAYLOAD_2       (8) // value0, value1
    #define LPP_SIZE_PAYLOAD_3      (12) // value0, value1, value2
    }

#endif

