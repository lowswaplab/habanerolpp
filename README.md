
# HabaneroLPP

HabaneroLPP is a Low Power Payload date type for IoT devices to send
data in small, simiple data packets.

HabaneroLPP is specifically design for use in low size, weight, and power
(SWaP) systems such as those using
[LoRa](https://en.wikipedia.org/wiki/LoRa) radios.

The samller the packets, the shorter the transmission time.  The shorter
the transmission time, the less power (e.g. battery) used.



# Platforms

HabaneroLPP has been ported to the Arduino and Linux development
environments.



# Source Code

[HabaneroLPP Git Repository](https://gitlab.com/lowswaplab/habanerolpp).



# Contribution

If you find this software useful, please consider financial support
for future development via
[PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY9C5FN54TCKL&source=url).



# Author

[Low SWaP Lab](https://www.lowswaplab.com/)



# Copyright

Copyright © 2020 Low SWaP Lab [lowswaplab.com](https://www.lowswaplab.com/)

