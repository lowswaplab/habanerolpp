
// HabaneroLPP
// https://gitlab.com/lowswaplab/habanerolpp
// Copyright © 2020 Low SWaP Lab lowswaplab.com

#include "version.h"
#include "HabaneroLPP.hpp"

#if defined(ARDUINO)
  #include <RadioHead.h> // for htons() and ntohs()
  #include <avr/dtostrf.h> // for dtostrf()
#elif defined(HAVE_LINUX)
  #include <sys/sysinfo.h>
  #include <sys/time.h>
#endif

using namespace LowSWaPLab;

HabaneroLPP::HabaneroLPP():
  _lpp()
  {
  reset();
  }

HabaneroLPP::~HabaneroLPP()
  {
  }

String HabaneroLPP::topic() const
  {
  switch (type())
    {
    case LPP_TYPE_ERROR:
      return(F("error"));
    case LPP_TYPE_ATTITUDE_HEADING:
      return(F("attitude/heading"));
    case LPP_TYPE_ATTITUDE_ROLL:
      return(F("attitude/roll"));
    case LPP_TYPE_ATTITUDE_PITCH:
      return(F("attitude/pitch"));
    case LPP_TYPE_ATTITUDE_TRACK:
      return(F("attitude/track"));
    case LPP_TYPE_ATTITUDE_XYZ:
      return(F("attitude/xyz"));
    case LPP_TYPE_STRING:
      return(F("string"));
    case LPP_TYPE_ENVIRO_HUMIDITY:
      return(F("enviro/humidity"));
    case LPP_TYPE_ENVIRO_PRESSURE:
      return(F("enviro/pressure"));
    case LPP_TYPE_ENVIRO_TEMPERATURE:
      return(F("enviro/temperature"));
    case LPP_TYPE_ENVIRO_THP:
      return(F("enviro/thp"));
    case LPP_TYPE_GPS_PDOP:
      return(F("gps/pdop"));
    case LPP_TYPE_GPS_POSITION:
      return(F("gps/position"));
    case LPP_TYPE_GPS_SATELLITES:
      return(F("gps/satellites"));
    case LPP_TYPE_LORA_NEIGHBOR:
      return(F("lora/neighbor"));
    case LPP_TYPE_LORA_NOISEFLOOR:
      return(F("lora/noisefloor"));
    case LPP_TYPE_LORA_RSSI:
      return(F("lora/rssi"));
    case LPP_TYPE_LORA_SNR:
      return(F("lora/snr"));
    case LPP_TYPE_SPEED_GROUND:
      return(F("speed/ground"));
    case LPP_TYPE_SPEED_VERTICAL:
      return(F("speed/vertical"));
    case LPP_TYPE_SYS_SERIALNUM:
      return(F("sys/serialnum"));
    case LPP_TYPE_SYS_UPTIME:
      return(F("sys/uptime"));
    case LPP_TYPE_TIMEOUT:
      return(F("timeout"));
    case LPP_TYPE_WIFI_RSSI:
      return(F("wifi/rssi"));
    case LPP_TYPE_VOLTAGE_DC:
      return(F("voltage/dc"));
    case LPP_TYPE_RPC_HLPP_VERSION_REPLY:
      return(F("rpc/hlpp_version_reply"));
    case LPP_TYPE_RPC_HLPP_VERSION_REQUEST:
      return(F("rpc/hlpp_version_request"));
    case LPP_TYPE_RPC_PING_REPLY:
      return(F("rpc/ping_reply"));
    case LPP_TYPE_RPC_PING_REQUEST:
      return(F("rpc/ping_request"));
    };

  return(F(""));
  }

uint8_t HabaneroLPP::type() const
  {
  return(_lpp.type);
  }

uint8_t HabaneroLPP::size() const
  {
  uint8_t _size;

  _size = LPP_SIZE_TYPE + LPP_SIZE_CHECKSUM;

  switch (type())
    {
    case LPP_TYPE_ATTITUDE_XYZ:
    case LPP_TYPE_ENVIRO_THP:
    case LPP_TYPE_GPS_POSITION:
      _size += LPP_SIZE_PAYLOAD_3;
      break;
    default:
      _size += LPP_SIZE_PAYLOAD_1;
      break;
    };

  return(_size);
  }

String HabaneroLPP::values() const
  {
  switch (type())
    {
    case LPP_TYPE_STRING:
      break;
    case LPP_TYPE_ATTITUDE_HEADING:
    case LPP_TYPE_ATTITUDE_ROLL:
    case LPP_TYPE_ATTITUDE_PITCH:
    case LPP_TYPE_ATTITUDE_TRACK:
    case LPP_TYPE_ENVIRO_TEMPERATURE:
    case LPP_TYPE_ENVIRO_HUMIDITY:
    case LPP_TYPE_ENVIRO_PRESSURE:
    case LPP_TYPE_LORA_NOISEFLOOR:
    case LPP_TYPE_LORA_RSSI:
    case LPP_TYPE_SPEED_GROUND:
    case LPP_TYPE_SPEED_VERTICAL:
    case LPP_TYPE_WIFI_RSSI:
    case LPP_TYPE_VOLTAGE_DC:
      {
      #if defined(ARDUINO)
        char buf[16];
        dtostrf(value0f(), 0, 2, buf);
        return(buf);
      #elif defined(HAVE_LINUX)
        char buf[16];
        snprintf(buf, 16, "%.1f", value0f());
        return(buf);
      #endif
      };
    case LPP_TYPE_ENVIRO_THP:
      {
      #if defined(ARDUINO)
        return(String(value0f(), 1) + F(",") + String(value1f(), 0) +
               F(",") + String(value2f(), 1));
      #elif defined(HAVE_LINUX)
        char buf[64];
        snprintf(buf, 64, "%.1f,%.0f,%.1f", value0f(), value1f(), value2f());
        return(buf);
      #endif
      };
    case LPP_TYPE_LORA_NEIGHBOR:
      {
      #if defined(ARDUINO)
        {
        String v0;

        v0 = String(value0u(), HEX);
        v0.toUpperCase();
        return("0x" + v0 + F(",") + String(value1f()));
        };
      #elif defined(HAVE_LINUX)
        char buf[64];
        snprintf(buf, 64, "0x%X,%.5f", value0u(), value1f());
        return(buf);
      #endif
      };
    case LPP_TYPE_LORA_SNR:
      {
      #if defined(ARDUINO)
        String v0;
        String v1;

        v0 = String(value0u(), HEX);
        v1 = String(value1u(), HEX);
        v0.toUpperCase();
        v1.toUpperCase();
        return("0x" + v0 + F(",0x") + v1 + F(",") + String(value2f(), 1));
      #elif defined(HAVE_LINUX)
        char buf[64];
        snprintf(buf, 64, "0x%X,0x%X,%.1f", value0u(), value1u(), value2f());
        return(buf);
      #endif
      }
    case LPP_TYPE_GPS_POSITION:
      {
      #if defined(ARDUINO)
        if (isinf(value2f()))
          return(String(value0f(), 5) + F(",") + String(value1f(), 5));
        return(String(value0f(), 5) + F(",") + String(value1f(), 5) +
               F(",") + String((int)round(value2f())));
      #elif defined(HAVE_LINUX)
        char buf[64];
        if (isinf(value2f()))
          snprintf(buf, 64, "%.5f,%.5f", value0f(), value1f());
        else
          snprintf(buf, 64, "%.5f,%.5f,%d", value0f(), value1f(),
                   (int)round(value2f()));
        return(buf);
      #endif
      };
    case LPP_TYPE_RPC_PING_REPLY:
      {
      #if defined(ARDUINO)
        return(String(value0u()) + F(",") + String(value1f(), 1));
      #elif defined(HAVE_LINUX)
        char buf[64];
        snprintf(buf, 64, "%u,%.1f", value0u(), value1f());
        return(buf);
      #endif
      };
    case LPP_TYPE_GPS_SATELLITES:
    case LPP_TYPE_SYS_SERIALNUM:
    case LPP_TYPE_SYS_UPTIME:
    case LPP_TYPE_TIMEOUT:
      {
      #if defined(ARDUINO)
        return(String(value0u()));
      #elif defined(HAVE_LINUX)
        return(std::to_string(value0u()));
      #endif
      };
    case LPP_TYPE_ATTITUDE_XYZ:
      {
      #if defined(ARDUINO)
        return(String(value0f(), 1) + F(",") + String(value1f(), 1) +
               F(",") + String(value2f(), 1));
      #elif defined(HAVE_LINUX)
        char buf[64];
        snprintf(buf, 64, "%.1f,%.1f,%.1f", value0f(), value1f(), value2f());
        return(buf);
      #endif
      };
    case LPP_TYPE_GPS_PDOP:
      {
      #if defined(ARDUINO)
        return(String(value0u()) + F(",") + String(value1u()) + F(",") +
               String(value2u()));
      #elif defined(HAVE_LINUX)
        char buf[64];
        snprintf(buf, 64, "%u,%u,%u", value0u(), value1u(), value2u());
        return(buf);
      #endif
      };
    };

  return("");
  }

float HabaneroLPP::value0f() const
  {
  return(_lpp.value0.f);
  }

float HabaneroLPP::value1f() const
  {
  return(_lpp.value1.f);
  }

float HabaneroLPP::value2f() const
  {
  return(_lpp.value2.f);
  }

uint32_t HabaneroLPP::value0u() const
  {
  return(_lpp.value0.u);
  }

uint32_t HabaneroLPP::value1u() const
  {
  return(_lpp.value1.u);
  }

uint32_t HabaneroLPP::value2u() const
  {
  return(_lpp.value2.u);
  }

int32_t HabaneroLPP::value0i() const
  {
  return(_lpp.value0.i);
  }

int32_t HabaneroLPP::value1i() const
  {
  return(_lpp.value1.i);
  }

int32_t HabaneroLPP::value2i() const
  {
  return(_lpp.value2.i);
  }

void HabaneroLPP::reset()
  {
  memset(&_lpp, 0, sizeof(_lpp));
  }

void HabaneroLPP::lpp(HabaneroLPP_t *packet)
  {
  memcpy((uint8_t *)packet, (uint8_t *)&_lpp, sizeof(_lpp));
  }

String HabaneroLPP::debug(const uint8_t *_buffer, const uint8_t _size)
  {
  String str="";
  uint8_t index;

  for (index=0; index < _size; ++index)
    {
    char buf[3];

    #if defined(ARDUINO)
      sprintf(buf, "%02X", _buffer[index]);
    #elif defined(HAVE_LINUX)
      snprintf(buf, 3, "%02X", _buffer[index]);
    #endif
    str += buf;
    if ((index+1) % 4 == 0)
      str += F(" ");
    };

  return(str);
  }

bool HabaneroLPP::parse(const uint8_t *_buffer, const uint8_t _size)
  {
  reset();

  // XXX double check _size with type
  if (0 == _size)
    {
    errno = EINVAL;
    return(false);
    };

  _lpp.csum = _buffer[0];
  _lpp.type = _buffer[1];
  memcpy(&_lpp.value0, _buffer+2, 4);
  memcpy(&_lpp.value1, _buffer+6, 4);
  memcpy(&_lpp.value2, _buffer+10, 4);

  // skip checksum spot in calculating checksum
  //_lpp.csum = checksum((uint8_t *)&_lpp+LPP_SIZE_CHECKSUM,
  //                     _size-LPP_SIZE_CHECKSUM);
  _lpp.csum = checksum(_buffer+LPP_SIZE_CHECKSUM, _size-LPP_SIZE_CHECKSUM);

  if (_buffer[0] != _lpp.csum)
    {
    reset();
    errno = EBADMSG;
    return(false);
    };

  return(true);
  }

bool HabaneroLPP::packet(uint8_t *_buffer, uint8_t *_size)
  {
  memset(_buffer, 0, *_size);

  if (*_size < LPP_SIZE_CHECKSUM+LPP_SIZE_TYPE+LPP_SIZE_PAYLOAD_3)
    {
    *_size = 0;
    errno = EINVAL;
    return(false);
    };

  *_size = size();

  _buffer[1] = type();
  memcpy(_buffer+2, (uint8_t *)&_lpp.value0, 4);
  memcpy(_buffer+6, (uint8_t *)&_lpp.value1, 4);
  memcpy(_buffer+10, (uint8_t *)&_lpp.value2, 4);

  // skip checksum spot in calculating checksum
  //_lpp.csum = checksum((uint8_t *)&_lpp+LPP_SIZE_CHECKSUM,
  //                     (*_size)-LPP_SIZE_CHECKSUM);
  _lpp.csum = checksum(_buffer+LPP_SIZE_CHECKSUM, *_size-LPP_SIZE_CHECKSUM);

  _buffer[0] = _lpp.csum;

  return(true);
  }

// https://stackoverflow.com/questions/31151032/writing-an-8-bit-checksum-in-c
uint8_t HabaneroLPP::checksum(const uint8_t *buffer, const uint8_t size)
  {
  uint8_t *ptr;
  uint16_t sum;
  uint8_t _size;

  ptr = (uint8_t *)buffer;
  _size = size;
  for (sum=0; _size !=0; _size--)
    sum += *(ptr++);

  while (sum > 0xFFU)
    sum = (sum & 0xFFU) + ((sum >> 8) & 0xFFU);

  return(sum);
  }

bool HabaneroLPP::set0(const uint8_t type, const float value0)
  {
  _lpp.type = type;
  _lpp.value0.f = value0;

  return(true);
  }
bool HabaneroLPP::set0(const uint8_t type, const uint32_t value0)
  {
  _lpp.type = type;
  _lpp.value0.u = value0;

  return(true);
  }
bool HabaneroLPP::set0(const uint8_t type, const int32_t value0)
  {
  _lpp.type = type;
  _lpp.value0.i = value0;

  return(true);
  }

bool HabaneroLPP::set1(const uint8_t type, const float value1)
  {
  _lpp.type = type;
  _lpp.value1.f = value1;

  return(true);
  }
bool HabaneroLPP::set1(const uint8_t type, const uint32_t value1)
  {
  _lpp.type = type;
  _lpp.value1.u = value1;

  return(true);
  }
bool HabaneroLPP::set1(const uint8_t type, const int32_t value1)
  {
  _lpp.type = type;
  _lpp.value1.i = value1;

  return(true);
  }

bool HabaneroLPP::set2(const uint8_t type, const float value2)
  {
  _lpp.type = type;
  _lpp.value2.f = value2;

  return(true);
  }
bool HabaneroLPP::set2(const uint8_t type, const uint32_t value2)
  {
  _lpp.type = type;
  _lpp.value2.u = value2;

  return(true);
  }
bool HabaneroLPP::set2(const uint8_t type, const int32_t value2)
  {
  _lpp.type = type;
  _lpp.value2.i = value2;

  return(true);
  }

bool HabaneroLPP::setAttitudeHeading(const float value)
  {
  if (value >= 0 && value < 360)
    return(set0(LPP_TYPE_ATTITUDE_HEADING, value));
  return(false);
  }

bool HabaneroLPP::setAttitudeRoll(const float value)
  {
  if (value >= 0 && value < 360)
    return(set0(LPP_TYPE_ATTITUDE_ROLL, value));
  return(false);
  }

bool HabaneroLPP::setAttitudePitch(const float value)
  {
  if (value >= 0 && value < 360)
    return(set0(LPP_TYPE_ATTITUDE_PITCH, value));
  return(false);
  }

bool HabaneroLPP::setAttitudeTrack(const float value)
  {
  if (value >= 0 && value < 360)
    return(set0(LPP_TYPE_ATTITUDE_TRACK, value));
  return(false);
  }

bool HabaneroLPP::setAttitudeXYZ(const float x, const float y, const float z)
  {
  if (x > -360 && x < 360 && y > -360 && y < 360 && z > -360 && z < 360)
    {
    bool ret0=false, ret1=false, ret2=false;

    ret0 = set0(LPP_TYPE_ATTITUDE_XYZ, x);
    ret1 = set1(LPP_TYPE_ATTITUDE_XYZ, y);
    ret2 = set2(LPP_TYPE_ATTITUDE_XYZ, z);

    if (ret0 && ret1 && ret2)
      return(true);
    };

  return(false);
  }

bool HabaneroLPP::setEnviroHumidity(const float value)
  {
  if (value >= 0 && value <= 100)
    return(set0(LPP_TYPE_ENVIRO_HUMIDITY, value));
  return(false);
  }

bool HabaneroLPP::setEnviroPressure(const float value)
  {
  if (value >= 0 && value <= 3.4E38)
    return(set0(LPP_TYPE_ENVIRO_PRESSURE, value));
  return(false);
  }

bool HabaneroLPP::setEnviroTemperature(const float value)
  {
  if (value >= -273.15 && value < 1.417E32)
    return(set0(LPP_TYPE_ENVIRO_TEMPERATURE, value));
  return(false);
  }

bool HabaneroLPP::setEnviroTHP(const float temperature, const float humidity,
                               const float pressure)
  {
  bool ret0=false, ret1=false, ret2=false;

  if (temperature >= -273.15 && temperature < 1.417E32)
    ret0 = set0(LPP_TYPE_ENVIRO_THP, temperature);
  if (humidity >= 0 && humidity <= 100)
    ret1 = set1(LPP_TYPE_ENVIRO_THP, humidity);
  if (pressure >= 0 && pressure <= 3.4E38)
    ret2 = set2(LPP_TYPE_ENVIRO_THP, pressure);

  if (ret0 && ret1 && ret2)
    return(true);

  return(false);
  }

bool HabaneroLPP::setGPSpdop(const uint32_t value0, const uint32_t value1,
                             const uint32_t value2)
  {
  bool ret0=false, ret1=false, ret2=false;

  ret0 = set0(LPP_TYPE_GPS_PDOP, value0);
  ret1 = set1(LPP_TYPE_GPS_PDOP, value1);
  ret1 = set2(LPP_TYPE_GPS_PDOP, value2);

  if (ret0 && ret1 && ret2)
    return(true);

  return(false);
  }

bool HabaneroLPP::setGPSposition(const float latitude, const float longitude,
                                 const float altitude)
  {
  // XXX limits for altitude??
  if (latitude >= -90 && latitude <= 90 &&
      longitude >= -180 && longitude <= 180)
    {
    bool ret0=false, ret1=false, ret2=false;

    ret0 = set0(LPP_TYPE_GPS_POSITION, latitude);
    ret1 = set1(LPP_TYPE_GPS_POSITION, longitude);
    ret2 = set2(LPP_TYPE_GPS_POSITION, altitude);

    if (ret0 && ret1 && ret2)
      return(true);

    return(false);
    };

  return(false);
  }

bool HabaneroLPP::setGPSsatellites(const uint32_t value)
  {
  return(set0(LPP_TYPE_GPS_SATELLITES, value));
  }

bool HabaneroLPP::setLoRaNeighbor(const uint32_t addr, const float rssi)
  {
  bool ret0, ret1=false;

  ret0 = set0(LPP_TYPE_LORA_NEIGHBOR, addr);
  if (rssi >= -179 && rssi <= 0)
    ret1 = set1(LPP_TYPE_LORA_NEIGHBOR, rssi);

  if (ret0 && ret1)
    return(true);

  return(false);
  }

bool HabaneroLPP::setLoRaNoisefloor(const float value)
  {
  if (value >= -179 && value <= 0)
    return(set0(LPP_TYPE_LORA_NOISEFLOOR, value));
  return(false);
  }

bool HabaneroLPP::setLoRaRSSI(const float value)
  {
  if (value >= -179 && value <= 0)
    return(set0(LPP_TYPE_LORA_RSSI, value));
  return(false);
  }

bool HabaneroLPP::setLoRaSNR(const uint32_t saddr, const uint32_t daddr,
                             const float value)
  {
  bool ret0, ret1, ret2=false;

  ret0 = set0(LPP_TYPE_LORA_SNR, saddr);
  ret1 = set1(LPP_TYPE_LORA_SNR, daddr);
  if (value >= -179 && value <= 179)
    ret2 = set2(LPP_TYPE_LORA_SNR, value);

  if (ret0 && ret1 && ret2)
    return(true);

  return(false);
  }

bool HabaneroLPP::setSpeedGround(const float value)
  {
  if (value >= 0)
    return(set0(LPP_TYPE_SPEED_GROUND, value));
  return(false);
  }

bool HabaneroLPP::setSpeedVertical(const float value)
  {
  return(set0(LPP_TYPE_SPEED_VERTICAL, value));
  }

bool HabaneroLPP::setSysSerialNum(const uint32_t value)
  {
  return(set0(LPP_TYPE_SYS_SERIALNUM, value));
  }

bool HabaneroLPP::setSysUptime(const uint32_t value)
  {
  if (value == 0)
    {
    #if defined(ARDUINO)
      return(set0(LPP_TYPE_SYS_UPTIME, (uint32_t)(millis()/1000)));
    #elif defined(HAVE_LINUX)
      {
      struct sysinfo info;

      sysinfo(&info);
      // XXX on linux, uptime is uint64_t!!!!
      return(set0(LPP_TYPE_SYS_UPTIME, (uint32_t)info.uptime));
      };
    #endif
    };

  return(set0(LPP_TYPE_SYS_UPTIME, value));
  }

bool HabaneroLPP::setTimeout(const uint32_t value)
  {
  return(set0(LPP_TYPE_TIMEOUT, value));
  }

bool HabaneroLPP::setVoltageDC(const float value)
  {
  return(set0(LPP_TYPE_VOLTAGE_DC, value));
  }

bool HabaneroLPP::setWiFiRSSI(const float value)
  {
  if (value >= -179 && value <= 0)
    return(set0(LPP_TYPE_WIFI_RSSI, value));
  return(false);
  }

bool HabaneroLPP::setRPCblink()
  {
  return(set0(LPP_TYPE_RPC_BLINK, (uint32_t)1));
  }

bool HabaneroLPP::setRPChlppVersionReply()
  {
  return(set0(LPP_TYPE_RPC_HLPP_VERSION_REPLY, HABANEROLPP_VERSION_F));
  }

bool HabaneroLPP::setRPChlppVersionRequest()
  {
  return(set0(LPP_TYPE_RPC_HLPP_VERSION_REQUEST, HABANEROLPP_VERSION_F));
  }

bool HabaneroLPP::setRPCpingReply(const uint32_t time, const float snr)
  {
  bool ret0;
  bool ret1;

  ret0 = set0(LPP_TYPE_RPC_PING_REPLY, time);
  ret1 = set1(LPP_TYPE_RPC_PING_REPLY, snr);

  if (ret0 && ret1)
    return(true);

  return(false);
  }

bool HabaneroLPP::setRPCpingRequest(const uint32_t value)
  {
  // XXX _value should be uint64_t (to try to avoid roll overs)
  uint32_t _value;

  _value = value;
  if (_value == 0)
    {
    #if defined(ARDUINO)
      _value = micros();
    #elif defined(HAVE_LINUX)
      {
      struct timeval tv;

      gettimeofday(&tv, NULL);
      _value = 1000000 * tv.tv_sec + tv.tv_usec;
      };
    #endif
    };

  return(set0(LPP_TYPE_RPC_PING_REQUEST, _value));
  }

