
// HabaneroLPP
// https://gitlab.com/lowswaplab/habanerolpp
// Copyright © 2020 Low SWaP Lab lowswaplab.com

#include <stdio.h>
#include <time.h>
#include <gps.h>
#include <libgpsmm.h>
#include <mosquitto.h>
#include <HabaneroLPP.hpp>

#define kn2kph(x) (x*1.852)

struct mosquitto *mosq=NULL;
const std::string topicPrefix= "lowswaplab.com/low/low/";

  bool mqttSend(const std::string &topic, const std::string &value,
                const bool retain=false, const bool debug=false)
    {
    if (mosq == NULL)
      return(false);

    std::string _topic;

    _topic = topicPrefix + topic;

    if (debug)
      fprintf(stderr, "MQTT: %s %s\n", _topic.c_str(), value.c_str());

    if (mosquitto_publish(mosq, NULL, _topic.c_str(), value.length(),
                         (void *)value.c_str(), 0, retain)
         != MOSQ_ERR_SUCCESS)
      {
      fprintf(stderr, "MQTT publish error\n");

      return(false);
      };

    return(true);
    }

int main(void)
  {
  time_t timeCurr;
  time_t timePrev=0;
  LowSWaPLab::HabaneroLPP lpp;
  std::string topic;

  topic = "lowswaplab.com/";

  // XXX name
  topic += "low/";

  mosquitto_lib_init();
  mosq = mosquitto_new(NULL, true, NULL);
  //if (mosquitto_connect(mosq, "192.168.1.3", 1883, 60))
  if (mosquitto_connect(mosq, "127.0.0.1", 1883, 60))
    {
    fprintf(stderr, "MQTT connection error\n");
    return(1);
    };
  if (mosquitto_loop_start(mosq) != MOSQ_ERR_SUCCESS)
    {
    fprintf(stderr, "MQTT loop error\n");
    return(1);
    };

  gpsmm gps_rec("localhost", DEFAULT_GPSD_PORT);

  if (gps_rec.stream(WATCH_ENABLE|WATCH_JSON) == NULL)
    {
    fprintf(stderr, "GPSD connection error\n");
    return(1);
    };

  while (true)
    {
    struct gps_data_t *gpsdata;

    timeCurr = time(NULL);

    if (!gps_rec.waiting(50000000))
      continue;

    if ((gpsdata=gps_rec.read()) == NULL)
      {
      fprintf(stderr, "GPSD read error\n");
      return(1);
      }
    else
      {
      // process new data
      //fprintf(stderr, "%s\n", gps_data(gpsdata));
      };

    // XXX timeout should be less if moving
    if (timeCurr - timePrev > 1)
      {
    /*
      lpp.setSysUptime();
      mqttSend(lpp.topic(), lpp.values(), false, true);
      */

      lpp.setTimeout(5);
      mqttSend(lpp.topic(), lpp.values(), false, true);

      if (gpsdata->fix.mode == MODE_2D)
        {
        if (lpp.setGPSposition(gpsdata->fix.latitude,
                               gpsdata->fix.longitude))
          {
          mqttSend(lpp.topic(), lpp.values(), false, true);
          };
        };
      if (gpsdata->fix.mode == MODE_3D)
        {
        if (lpp.setGPSposition(gpsdata->fix.latitude,
                               gpsdata->fix.longitude,
                               gpsdata->fix.altitude))
          {
          mqttSend(lpp.topic(), lpp.values(), false, true);
          };
        };

      if (gpsdata->fix.mode >= MODE_2D)
        {
        if (lpp.setGPSpdop(gpsdata->dop.pdop))
          mqttSend(lpp.topic(), lpp.values(), false, true);

        if (lpp.setSpeedGround(kn2kph(gpsdata->fix.speed)))
          mqttSend(lpp.topic(), lpp.values(), false, true);

        // only report track if moving
        // XXX is x kph fast enough?
        if (kn2kph(gpsdata->fix.speed) >= 0.5)
          {
          if (lpp.setAttitudeTrack(gpsdata->fix.track))
            mqttSend(lpp.topic(), lpp.values(), false, true);
          };
        };

      timePrev = timeCurr;
      };
    };
  }

