
// HabaneroLPP
// https://gitlab.com/lowswaplab/habanerolpp
// Copyright © 2020 Low SWaP Lab lowswaplab.com

// Requires:
// a Moteino or MoteinoMEGA
// RadioHead library
// LoRa or similar radio
// LowSWaPLab's Particulate filter library

// Set up two nodes and they'll ping each other.

// Just for fun, we attempt to smooth the results with an
// αβ and CMA filters

#include <RH_RF69.h>
#include <RHReliableDatagram.h>
#include <HabaneroLPP.hpp>
#include <Particulate.hpp>

#define READING_TIMEOUT (15)   // seconds

// key must be exactly 16 bytes and same on all nodes
uint8_t rfmKey[] = " lowswaplab.com ";

#ifdef MOTEINO
  #define RFM_SS   (SS)
  #define RFM_IRQ  (2)
#endif
#ifdef MOTEINO_MEGA
  #define RFM_SS   (4)
  #define RFM_IRQ  (2)
#endif

#define LORA_ADDR (1)
RH_RF69 rfm(RFM_SS, RFM_IRQ);
RHReliableDatagram lora(rfm);
uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
uint8_t buflen=sizeof(buf);

LowSWaPLab::HabaneroLPP lpp;

//LowSWaPLab::AlphaBeta abRTT;
LowSWaPLab::CMA cmaRTT;
LowSWaPLab::CMA cmaRSSI;

uint32_t secs;
uint32_t secsReading=0;

bool sendLPP(uint8_t to, LowSWaPLab::HabaneroLPP &_lpp)
  {
  uint8_t buflen;

  buflen = RH_RF69_MAX_MESSAGE_LEN;
  _lpp.packet(buf, &buflen);

  Serial.print("RFM TX ");
  Serial.print(rfm.rssiRead());
  Serial.print(" dBm: 0x");
  Serial.print(to, HEX);
  Serial.print(" ");
  Serial.print(_lpp.size());
  Serial.print("B ");
  Serial.print(_lpp.type());
  Serial.print("T ");
  Serial.print(lpp.topic());
  Serial.println();

  return(lora.sendto(buf, buflen, to));
  }

bool sendRFMheartbeat()
  {
  lpp.setLoRaNoisefloor(rfm.rssiRead());
  sendLPP(RH_BROADCAST_ADDRESS, lpp);

  //lpp.setGPSposition(81, 91, 55);
  //sendLPP(RH_BROADCAST_ADDRESS, lpp);

  lpp.setRPCpingRequest(micros());
  sendLPP(RH_BROADCAST_ADDRESS, lpp);

  lpp.setRPChlppVersionRequest();
  sendLPP(RH_BROADCAST_ADDRESS, lpp);

  return(true);
  }

void setup()
  {
  Serial.begin(115200);

  delay(1000); // wait for serial port
  Serial.println("");
  Serial.println("");
  Serial.println("Low SWaP Lab");
  Serial.println("lowswaplab.com");
  Serial.println("");
  Serial.println("");

  #ifdef MOTEINO
    Serial.println("MOTEINO");
  #endif
  #ifdef MOTEINO_MEGA
    Serial.println("MOTEINO MEGA");
  #endif

  //abRTT.setup(0.5, 0.005, 0.5);

  while (!lora.init())
    {
    Serial.println("LoRa initialization error");
    delay(5000);
    };

  lora.setThisAddress(LORA_ADDR);

  rfm.setTxPower(20, true);
  rfm.setFrequency(915.0);
  rfm.setEncryptionKey(rfmKey);

  sendRFMheartbeat();
  }

void loop()
  {
  secs = millis() / 1000;

  if (secs-secsReading > READING_TIMEOUT)
    {
    sendRFMheartbeat();

    secsReading = secs;
    };

  if (lora.waitAvailableTimeout(250))
    {
    uint8_t from=0;

    buflen = sizeof(buf);
    if (lora.recvfrom(buf, (uint8_t *)&buflen, &from))
      {
      uint8_t type=0;
      float value0f=0;
      float value1f=0;
      float value2f=0;
      uint32_t value0u=0;
      float rssi;
      float noisefloor;
      float snr;

      rssi = rfm.lastRssi();
      noisefloor = rfm.rssiRead();
      snr = rssi - noisefloor;

      lpp.parse(buf, buflen);
      type = lpp.type();
      value0f = lpp.value0f();
      value1f = lpp.value1f();
      value2f = lpp.value2f();
      value0u = lpp.value0u();

      Serial.print("RFM RX ");
      Serial.print(rssi);
      Serial.print(" dBm: 0x");
      Serial.print(from, HEX);
      Serial.print(" ");
      Serial.print(buflen);
      Serial.print("B ");
      Serial.print(type);
      Serial.print("T ");
      switch (type)
        {
        case LowSWaPLab::LPP_TYPE_GPS_POSITION:
          Serial.print(value0f);
          Serial.print(" ");
          Serial.print(value1f);
          Serial.print(" ");
          Serial.print(value2f);
          break;
        case LowSWaPLab::LPP_TYPE_RPC_PING_REQUEST:
          Serial.print(value0u);
          Serial.print("μs");
          break;
        case LowSWaPLab::LPP_TYPE_RPC_PING_REPLY:
          Serial.print(value0u);
          Serial.print("μs ");
          Serial.print(value1f);
          Serial.print("dBm");
          break;
        default:
          Serial.print(value0f);
          break;
        };
      Serial.print(" ");
      Serial.print(lpp.topic());
      Serial.println();

      // if received ping request, send ping reply
      if (LowSWaPLab::LPP_TYPE_RPC_PING_REQUEST == type)
        {
        lpp.setRPCpingReply(lpp.value0u(), snr);
        sendLPP(from, lpp);
        };

      // if received HabaneroLPP version request, send reply
      if (LowSWaPLab::LPP_TYPE_RPC_HLPP_VERSION_REQUEST == type)
        {
        lpp.setRPCpingReply(lpp.value0u(), snr);
        sendLPP(from, lpp);
        };

      // if received ping reply, calculate round trip time
      // NOTE!  This RTT includes group delays introduced by both the
      // sender and receiver processors doing additional things.
      if (LowSWaPLab::LPP_TYPE_RPC_PING_REPLY == type)
        {
        uint32_t sent;
        uint32_t curr;
        double rssi_cma;

        rssi_cma = cmaRSSI.update(rssi);

        sent = lpp.value0u();
        curr = micros();

        // XXX check for micros() rollover
        if (curr < sent)
          {
          //abRTT.reset();
          cmaRTT.reset();
          }
        else
          {
          uint32_t rtt;
          //double rtt_ab;
          double rtt_cma;

          rtt = curr - sent;
          //rtt_ab = abRTT.update(rtt);
          rtt_cma = cmaRTT.update(rtt);

          Serial.print("  PING REPLY RTT: ");
          Serial.print(rtt);
          Serial.print("μs ");
          //Serial.print(" αβ ");
          //Serial.print(rtt_ab);
          Serial.print("CMA: ");
          Serial.print(rtt_cma);
          Serial.print("μs ");
          Serial.print(rssi_cma);
          Serial.print("dBm");
          Serial.println();
          };
        };
      };
    };
  }

